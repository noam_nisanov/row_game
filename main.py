
import sys
import re

from RowGame import RowGameEngine


def main(player_x_module, player_o_module, json_fh):
    # strip .py ending if present
    clean_x_module_name = re.findall("(.+)\\.py$", player_x_module)
    clean_o_module_name = re.findall("(.+)\\.py$", player_o_module)

    if len(clean_x_module_name) >= 1:
        player_x_module = clean_x_module_name[0]
    if len(clean_o_module_name) >= 1:
        player_o_module = clean_o_module_name[0]

    # Load bot library
    player_x_bot = getattr(__import__(player_x_module), player_x_module)
    player_o_bot = getattr(__import__(player_o_module), player_o_module)

    # Create Game Engine
    rg = RowGameEngine(10, 10, 5, player_x_bot(), player_o_bot())
    rg.start_game(json_fh=json_fh, propogate_crash=True)


if __name__ == "__main__":
    out = sys.stdout
    if len(sys.argv) >= 4:
        out = open(sys.argv[3], "w")

    main(sys.argv[1], sys.argv[2], out)
