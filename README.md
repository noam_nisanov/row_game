
# Row Game

A game where you are required to complete ```N``` pieces in a row to win 
on a ```AxB``` board.

# Rules

* The board is of size ```AxB```.
* Any position on the board can contain 1 of 3 values:
  * ```x``` => piece of ```x``` player.
  * ```o``` => piece of ```o``` player.
  * ```-``` => empty position.
* The game is over once either player has ```N``` pieces in a row, where ```N``` is defined at the beginning, can be any value bigger then 1.
* No placing pieces on an already placed position.
* No placing pieces on a position outside the board boundries.

<b>Any violation of the rules will cause in the immediet disqualification of the bot</b>.

# Run

To test the bot you have created localy, run

```bash
$ python main.py BOT_PACKAGE_1 BOT_PACKAGE_2
```

# API

## Bot

To create bot for this game, you are required to create a bot class that will be used all along the game. The bot will only be required to contain 1 method named ```play(game)```, ```game``` being instance of class ```RowGamePlayer``` (see documentation).

### ```play(game)```

this method will be called automatically by the Engine every time it's the bot turn to make it's play.

**NOTE:** this method is to return a position (array) of 2 items ```[x,y]``` which is the bot decision where to place it's piece in it's turn. Failing to return this will cause the disqualification of the bot.

## RowGamePlayer Class

### ```get_pos(x, y)```

Returns the value of the position ```[x,y]```

### ```get_height()```

Returns the height of the board (i.e. value ```y```)

### ```get_width()```

Returns the width of the board (i.e. value ```x```)

### ```is_valid_move(x, y)```

Returns whether possible to place a piece at position ```[x,y]```.

### ```my_turn()```

Returns own bot sign (```x``` or ```o```).

### ```enemy_turn()```

Returns enemy bot sign (```x``` or ```o```).

### ```get_row_required()```

Returns the amount of pieces in a row required to achieve victory.

### ```get_my_positions()```

Returns all the pieces own bot has placed.

### ```get_enemy_positions()```

Returns all the pieces enemy bot has placed.

### ```debug(*args)```

Debugs a message that could be displayed later on
