
import json
import sys

from termcolor import colored


class GameJsonEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


class RowGameEngine(object):

    def __init__(self, width, height, row_required, player_x, player_o):
        super().__init__()
        self.history = []
        self.width = width
        self.height = height
        self.row_required = row_required
        self.players_bots = {
            'x': player_x,
            'o': player_o
        }

        self.players_positions = {
            'x': [],
            'o': [],
        }

        self.empty_pos = '-'

        self.game = [[self.empty_pos for _ in range(self.height)]
                     for _ in range(self.width)]

        self.player_turn = 'x'

    def _advance_player(self):
        if self.player_turn == 'x':
            self.player_turn = 'o'
        else:
            self.player_turn = 'x'

    def _get_player_bot(self):
        return self.players_bots[self.player_turn]

    def start_game(self, json_fh, propogate_crash=True):
        while True:
            cur_player = self._get_player_bot()
            cur_game_instance = RowGamePlayer(self, self.player_turn)
            if len(self.available_positions_amount()) == 0:
                print(colored("No Positions Left! Tie!", "blue"), file=sys.stderr)
                break
            try:
                chosen_move = cur_player.play(cur_game_instance)

                # chosen_move = cur_game_instance._move
                is_player_kicked = False
                if chosen_move[0] is None or chosen_move[1] is None:
                    is_player_kicked = True
                elif not self.is_valid_move(chosen_move[0], chosen_move[1]):
                    is_player_kicked = True

                if is_player_kicked:
                    raise Exception(
                        f"player {self.player_turn} failed! invalid move: {chosen_move}")
                else:
                    self.force_move(
                        chosen_move[0], chosen_move[1], self.player_turn)
            except Exception as e:
                print(
                    colored(
                        f"Player {self.player_turn} is Crashed! Lost automatically: {e}", "red"),
                    file=sys.stderr)
                raise e
                # exit(1)

            self.history.append(
                GameTurn(self, self.player_turn, chosen_move, cur_game_instance._debug))

            if self.is_game_over(chosen_move):
                break

            self._advance_player()

        print(f"Game Over!", file=sys.stderr)
        print(f"Winner is: {self.player_turn}", file=sys.stderr)
        print(GameJsonEncoder().encode(self.history), file=json_fh)

    def is_game_over(self, cur_pos):
        player_turn = self.get_pos(cur_pos[0], cur_pos[1])
        directions = []
        for x in range(-1, 2):
            for y in range(-1, 2):
                if x == 0 and y == 0:
                    continue
                directions.append([x, y])

        for cur_direction in directions:
            if self._check_win_over_direction(cur_pos[0],
                                              cur_pos[1],
                                              player_turn,
                                              cur_direction[0],
                                              cur_direction[1],
                                              0):
                return True
        return False

    def _check_win_over_direction(self, x, y, player_turn, dir_x, dir_y, count):
        if count == self.row_required:
            return True

        if not self.is_within_boundry(x, y):
            return False
        elif not player_turn == self.get_pos(x, y):
            return False

        return self._check_win_over_direction(x + dir_x,
                                              y + dir_y,
                                              player_turn,
                                              dir_x,
                                              dir_y,
                                              count + 1)

    def get_pos(self, x, y):
        return self.game[x][y]

    def available_positions_amount(self):
        available = []
        for x in range(self.width):
            for y in range(self.height):
                if not self.is_pos_taken(x, y):
                    available.append([x, y])
        return available

    def is_pos_taken(self, x, y):
        return self.game[x][y] != self.empty_pos

    def is_valid_move(self, x, y):
        if not self.is_within_boundry(x, y):
            return False
        return not self.is_pos_taken(x, y)

    def is_within_boundry(self, x, y):
        if x < 0 or y < 0:
            return False
        elif x >= self.width or y >= self.height:
            return False
        return True

    def force_move(self, x, y, player_turn):
        self.game[x][y] = player_turn
        self.players_positions[player_turn].append([x, y])

    def get_enemy_turn(self, player_turn):
        if player_turn == 'x':
            return 'o'
        else:
            return 'x'


class GameTurn(object):
    def __init__(self, game, player_turn, chosen_move, debug):
        super().__init__()
        self.game_snapshot = [[game.get_pos(x, y) for y in range(game.height)]
                              for x in range(game.width)]
        self.player_turn = player_turn
        self.chosen_move = chosen_move
        self.debug = debug


class RowGamePlayer(object):
    def __init__(self, row_game, player_turn):
        super().__init__()
        self._game = row_game
        # self._move = [None, None]

        self._debug = []
        self._my_turn = player_turn
        self._enemy_turn = self._game.get_enemy_turn(player_turn)

    def debug(self, *args):
        final_string = " ".join(args)
        print(final_string)
        self._debug.append(final_string)

    def get_pos(self, x, y):
        return self._game.get_pos(x, y)

    def get_height(self):
        return self._game.height

    def get_width(self):
        return self._game.width

    # def move(self, x, y):
    #     self._move = [x, y]

    def is_valid_move(self, x, y):
        return self._game.is_valid_move(x, y)

    def my_turn(self):
        return self._my_turn

    def enemy_turn(self):
        return self._enemy_turn

    def get_row_required(self):
        return self._game.row_required

    def get_my_positions(self):
        return [[p[0], p[1]] for p in self._game.players_positions[self.my_turn()]]

    def get_enemy_positions(self):
        return [[p[0], p[1]] for p in self._game.players_positions[self.enemy_turn()]]
