
import sys
import json
import time
import os

from termcolor import colored


def main(json_filename):
    history = json.load(open(json_filename, "r"))
    # turns = [i["game_snapshot"] for i in val]
    for turn in history:
        clearscreen()

        last_turn = turn["chosen_move"]
        map = turn["game_snapshot"]
        for y in range(len(map)):
            cur_row = []
            for x in range(len(map[y])):
                if x == last_turn[0] and y == last_turn[1]:
                    cur_row.append(colored(map[x][y], "green"))
                else:
                    cur_row.append(map[x][y])
            print(" ".join(cur_row))

        print("\n\n")
        for msg in turn["debug"]:
            print(msg)
        time.sleep(0.5)


def clearscreen(numlines=100):
    """Clear the console.
  numlines is an optional argument used only as a fall-back.
  """
# Thanks to Steven D'Aprano, http://www.velocityreviews.com/forums

    if os.name == "posix":
        # Unix/Linux/MacOS/BSD/etc
        os.system('clear')
    elif os.name in ("nt", "dos", "ce"):
        # DOS/Windows
        os.system('CLS')
    else:
        # Fallback for other operating systems.
        print('\n' * numlines)


main(sys.argv[1])
