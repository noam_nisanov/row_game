
class UserPlayer:
    """
    An example bot that makes moves from user himself, in form:
    x,y
    """

    def __init__(self):
        super().__init__()

    def play(self, game):
        print(f"MyPos: {game.get_my_positions()}")
        print(f"EnPos: {game.get_enemy_positions()}")
        for y in range(game.get_height()):
            cur_row = []
            for x in range(game.get_width()):
                cur_row.append(game.get_pos(x, y))
            print(" ".join(cur_row))

        move_str = input("Enter Move: ")
        move = move_str.split(",")
        return [int(move[0]), int(move[1])]
